Public domain.  No restriction, no need to accredit

> *As freely as you have received, freely give*  —  Jesus



## ABOUT

`cppp` is a c-pre-pre-processor for standard c macros, and `$exec script`

Best not explained by video

[![silly film](https://gratis.gitlab.io/aArray/cppp.jpg)](https://vimeo.com/186404784)

Why not just use the c pre-processor?  One usage could be auto-generating c headers:
```
$exec echo "// This file is generated" > myheader.h
$define EXPORT(...)
$exec echo "__VA_ARGS__;" >> myheader.h
__VA_ARGS__

EXPORT(void public_func())  { ... }
EXPORT(void public_other()) { ... }
```

You can use `$exec` to compress images, or anything else.  I use it to generate a website from source, push to a repository, etc

Why did you write it?  If c macros are involved in an error, compilers include information about them.  This can be messy.  `cppp` selectively pre-compiles them away



## USAGE
Replace any `#define` you want with `$define`.  Begin them on a new line and end them with a blank line.  This allows macros of macros, multiline macros, and stops the need to join lines with `\`

```
$define MACRO value

$define FUNCTION(name, ...) name __VA_ARGS__
```

These can end with a single line:

```
$include a/file/path

$exec shell script text
```

Also:
`$$` becomes `$`

`$##` concatonates like `##`

Then run with:  `cppp inputFile outputFile`



## INSTALL
Make sure you have [ninja](https://ninja-build.org) installed

Then use the command line to run:
```
git clone git@gitlab.com:gratis/cppp.git
cd cppp
ninja
```

You may then want to install it, with something like:
```
cp build/cppp /usr/local/bin/cppp
```



## OTHER
See test/ for usage examples

Function param `,` and `)` can be escaped with `\`

$exec newlines can be removed with `\`newline

```__VAR_ARGS__``` can be passed zero items.  This is not standards conforming, but occasionally useful
